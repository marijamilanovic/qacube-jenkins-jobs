package com.qacube.jenkinsjobs.controller;

import com.qacube.jenkinsjobs.domain.Notification;
import com.qacube.jenkinsjobs.domain.dto.ConnectionDto;
import com.qacube.jenkinsjobs.domain.dto.JobDto;
import com.qacube.jenkinsjobs.service.JobService;
import com.qacube.jenkinsjobs.service.interfaces.IConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class WebSocketController {

    private static final String SENDING_URL = "/topic/server-broadcaster";
    private static final String RECEIVING_URL = "/server-receiver";
    private final SimpMessagingTemplate template;

    @Autowired
    private JobService jobService;
    @Autowired
    private IConnectionService connectionService;

    @Autowired
    public WebSocketController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @SubscribeMapping(SENDING_URL)
    public Notification onSubscribe() {
        return new Notification("Connected", new Date());
    }

    @Scheduled(fixedRate = 5000)
    public void sendMessage() {
        Date now = new Date();
        List<JobDto> jobs = new ArrayList<>();

        for(ConnectionDto conn: connectionService.findAll()){
            if(conn.isEnableAuthentication()) {
                jobs = jobService.jobs(conn.getId(), conn.getUsername(), conn.getPassword());
            }else{
                jobs = jobService.jobs(conn.getId());
            }
            for(JobDto job: jobs){
                if(job.getLastBuild() != null && job.getLastBuild().getTimestamp() < now.getTime() && job.getLastBuild().getTimestamp() > now.getTime()-5000)
                    template.convertAndSend(SENDING_URL, new Notification("New job in " + conn.getName(), now));
            }
        }
    }

}
