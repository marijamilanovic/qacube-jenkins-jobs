package com.qacube.jenkinsjobs.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Connection implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Name can't be empty.")
    @Column(nullable = false)
    private String name;
    @NotBlank(message = "Url can't be empty.")
    @Column(nullable = false)
    private String url;
    @Column
    private String username;
    @Column
    private String password;
    @NotNull(message="Application id can't be empty.")
    @Column(nullable = false)
    private Long applicationId;
    @Column
    private boolean enableAuthentication = false;
}
