package com.qacube.jenkinsjobs.domain.dto;

import lombok.Data;

@Data
public class JobDto {
    private String _class;
    private String name;
    private LastBuildDto lastBuild;
}
