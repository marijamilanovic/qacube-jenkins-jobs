package com.qacube.jenkinsjobs.service.interfaces;

import com.qacube.jenkinsjobs.domain.dto.JobDto;

import java.util.List;

public interface IJobService {
    List<JobDto> jobs(Long connectionId, String username, String password);
    List<JobDto> jobs(Long connectionId);
}
