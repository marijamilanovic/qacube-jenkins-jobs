package com.qacube.jenkinsjobs.service.fetcher;

import com.qacube.jenkinsjobs.repository.IApplicationRepository;
import com.qacube.jenkinsjobs.repository.IConnectionRepository;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GraphQLDataFetchers {
    @Autowired
    private IApplicationRepository applicationRepository;
    @Autowired
    private IConnectionRepository connectionRepository;


    public DataFetcher getAppliciationByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            double id = dataFetchingEnvironment.getArgument("id");
            System.out.println("Id je:" + id);
            return applicationRepository.findById((long) id);
        };
    }

    public DataFetcher getAllApplicationsDataFetcher() {
        return dataFetchingEnvironment -> applicationRepository.findAll();
    }

    public DataFetcher getAllConnectionsDataFetcher() {
        return dataFetchingEnvironment -> connectionRepository.findAll();
    }

    public DataFetcher getConnectionByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            double id = dataFetchingEnvironment.getArgument("id");
            System.out.println("Id je:" + id);
            return connectionRepository.findById((long) id);
        };
    }


}
