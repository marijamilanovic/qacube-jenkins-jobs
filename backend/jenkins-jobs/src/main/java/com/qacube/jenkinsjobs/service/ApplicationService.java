package com.qacube.jenkinsjobs.service;

import com.qacube.jenkinsjobs.domain.Application;
import com.qacube.jenkinsjobs.exception.BadRequestException;
import com.qacube.jenkinsjobs.repository.IApplicationRepository;
import com.qacube.jenkinsjobs.service.interfaces.IApplicationService;
import com.qacube.jenkinsjobs.service.interfaces.IConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationService implements IApplicationService {
    @Autowired
    private IApplicationRepository applicationRepository;
    @Autowired
    private IConnectionService connectionService;

    @Override
    public Application create(Application application) {
        if(applicationRepository.findApplicationByName(application.getName()) != null)
            throw new BadRequestException("Application with that name exist.");
        return applicationRepository.save(application);
    }

    @Override
    public List<Application> findAll() {
        return applicationRepository.findAll();
    }

    @Override
    public Application findById(Long id){
        return applicationRepository.findApplicationById(id);
    }

    @Override
    public void delete(Long id){
        Application application = findById(id);
        if(application == null)
            throw new BadRequestException("Application does not exist.");
        connectionService.deleteConnectionsByApplication(id);
        applicationRepository.delete(application);
    }

    @Override
    public Application update(Application updateApplication){
        Application application = applicationRepository.findApplicationById(updateApplication.getId());
        if(applicationRepository.findApplicationByName(updateApplication.getName()) != null)
            throw new BadRequestException("Application with that name exist.");
        application.setName(updateApplication.getName());
        return applicationRepository.save(application);
    }


}
