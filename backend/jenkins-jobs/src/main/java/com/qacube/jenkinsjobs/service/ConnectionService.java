package com.qacube.jenkinsjobs.service;

import com.qacube.jenkinsjobs.domain.Connection;
import com.qacube.jenkinsjobs.domain.dto.ConnectionDto;
import com.qacube.jenkinsjobs.exception.BadRequestException;
import com.qacube.jenkinsjobs.exception.NotFoundException;
import com.qacube.jenkinsjobs.mapper.ConnectionMapper;
import com.qacube.jenkinsjobs.repository.IConnectionRepository;
import com.qacube.jenkinsjobs.service.interfaces.IApplicationService;
import com.qacube.jenkinsjobs.service.interfaces.IConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConnectionService implements IConnectionService {
    @Autowired
    private IConnectionRepository connectionRepository;
    @Autowired
    private IApplicationService applicationService;

    @Override
    public Connection create(Connection connection) {
        if(connectionRepository.findConnectionByName(connection.getName()) != null)
            throw new BadRequestException("Connection with that name already exist.");
        if(connectionRepository.findConnectionByUrl(connection.getUrl()) != null)
            throw new BadRequestException("Connection with that url already exist.");
        return connectionRepository.save(connection);
    }

    @Override
    public List<ConnectionDto> findAll() {
        List<Connection> connections = connectionRepository.findAll();
        List<ConnectionDto> connectionDtos = new ArrayList<>();
        if(!connections.isEmpty()){
            for(Connection c: connections){
                connectionDtos.add(ConnectionMapper.mapConnectionToConnectionDto(c, applicationService.findById(c.getApplicationId()).getName()));
            }
        }
        return connectionDtos;
    }

    @Override
    public ConnectionDto authentication(Long id, String username, String password){
        Connection connection = connectionRepository.findConnectionById(id);
        ConnectionDto connectionDto = new ConnectionDto();
        if(connection != null){
            if(username.equals(connection.getUsername()) && password.equals(connection.getPassword()))
                return ConnectionMapper.mapConnectionToConnectionDto(connection, applicationService.findById(connection.getId()).getName());
            throw  new NotFoundException("Username or password is not valid.");
        }
        throw new NotFoundException("Connection doesn't exist.");
    }

    @Override
    public Connection findById(Long id){
        return connectionRepository.findConnectionById(id);
    }

    @Override
    public Connection update(Connection updateConnection){
        Connection connection = connectionRepository.findConnectionById(updateConnection.getId());
        connection.setName(updateConnection.getName());
        connection.setUrl(updateConnection.getUrl());
        connection.setEnableAuthentication(updateConnection.isEnableAuthentication());
        connection.setApplicationId(updateConnection.getApplicationId());
        if(updateConnection.isEnableAuthentication()){
            connection.setUsername(updateConnection.getUsername());
            connection.setPassword(updateConnection.getPassword());
        }else{
            connection.setUsername("");
            connection.setPassword("");
        }
        return connectionRepository.save(connection);
    }

    @Override
    public void delete(Long id){
        Connection connection = findById(id);
        connectionRepository.delete(connection);
    }

    @Override
    public void deleteConnectionsByApplication(Long id){
        List<Connection> connections = connectionRepository.findConnectionByApplicationId(id);
        if(connections != null){
            for(Connection c: connections)
                delete(c.getId());
        }
    }

}
