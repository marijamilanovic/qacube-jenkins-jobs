package com.qacube.jenkinsjobs.repository;

import com.qacube.jenkinsjobs.domain.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IApplicationRepository extends JpaRepository<Application, Long> {
    Application save(Application application);
    List<Application> findAll();
    Application findApplicationById(Long id);
    Application findApplicationByName(String name);
}
