package com.qacube.jenkinsjobs.controller;

import com.qacube.jenkinsjobs.domain.dto.JobDto;
import com.qacube.jenkinsjobs.service.interfaces.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/job")
@CrossOrigin(origins = "http://localhost:4200")
public class JobController {
    @Autowired
    private IJobService jobService;

    @GetMapping("/{id}/{username}/{password}")
    public List<JobDto> jobs(@PathVariable("id")Long connectionId, @PathVariable("username") String username, @PathVariable("password") String password){
        return jobService.jobs(connectionId, username, password);
    }

    @GetMapping("/{id}")
    public List<JobDto> jobs(@PathVariable("id")Long connectionId){
        return jobService.jobs(connectionId);
    }
}
