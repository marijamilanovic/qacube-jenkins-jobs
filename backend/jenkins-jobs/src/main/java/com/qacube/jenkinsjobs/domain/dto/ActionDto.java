package com.qacube.jenkinsjobs.domain.dto;

import lombok.Data;

@Data
public class ActionDto {
    private LastBuiltRevisionDto lastBuiltRevision;
}
