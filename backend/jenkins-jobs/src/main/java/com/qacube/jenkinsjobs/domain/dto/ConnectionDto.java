package com.qacube.jenkinsjobs.domain.dto;

import lombok.Data;

@Data
public class ConnectionDto {
    private Long id;
    private String name;
    private String url;
    private String username;
    private String password;
    private String applicationName;
    private boolean enableAuthentication = false;
}
