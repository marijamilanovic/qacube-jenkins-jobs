package com.qacube.jenkinsjobs.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class LastBuiltRevisionDto {
    private List<BranchDto> branch;
}
