package com.qacube.jenkinsjobs.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class LastBuildDto {
    private Long duration;
    private String result;
    private Long timestamp;
    private List<ActionDto> actions;
}
