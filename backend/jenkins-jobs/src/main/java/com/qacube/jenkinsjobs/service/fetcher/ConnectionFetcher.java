package com.qacube.jenkinsjobs.service.fetcher;

import com.qacube.jenkinsjobs.domain.Connection;
import com.qacube.jenkinsjobs.repository.IConnectionRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ConnectionFetcher implements DataFetcher<List<Connection>> {
    @Autowired
    private IConnectionRepository connectionRepository;

    @Override
    public List<Connection> get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
        return connectionRepository.findAll();
    }
}
