package com.qacube.jenkinsjobs.controller;

import com.qacube.jenkinsjobs.domain.Connection;
import com.qacube.jenkinsjobs.domain.dto.ConnectionDto;
import com.qacube.jenkinsjobs.service.provider.GraphQLProvider;
import com.qacube.jenkinsjobs.service.interfaces.IConnectionService;
import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/connection")
@CrossOrigin(origins = "http://localhost:4200")
public class ConnectionController {
    @Autowired
    private IConnectionService connectionService;


    @GetMapping
    public List<ConnectionDto> findAll(){
        return connectionService.findAll();
    }

    @PostMapping
    public Connection create(@RequestBody Connection connection){
        return connectionService.create(connection);
    }

    @GetMapping("/{id}")
    public Connection findById(@PathVariable("id") Long id){
        return connectionService.findById(id);
    }

    @GetMapping("/{id}/{username}/{password}")
    public ConnectionDto authentication(@PathVariable("id") long id, @PathVariable("username") String username, @PathVariable("password") String password){
        return connectionService.authentication(id, username, password);
    }

    @PutMapping
    public Connection update(@RequestBody Connection updateConnection) {
        return connectionService.update(updateConnection);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") long id){
        connectionService.delete(id);
    }


    // GraphQL

    @Autowired
    private GraphQLProvider graphQLProvider;

    @PostMapping("/graphql")
    public ResponseEntity<Object> findAll(@RequestBody String query){
        ExecutionResult execute = graphQLProvider.graphQL().execute(query);
        return new ResponseEntity<>(execute, HttpStatus.OK);
    }


}
