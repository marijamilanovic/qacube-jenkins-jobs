package com.qacube.jenkinsjobs.service.fetcher;

import com.qacube.jenkinsjobs.domain.Application;
import com.qacube.jenkinsjobs.repository.IApplicationRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ApplicationFetcher implements DataFetcher<List<Application>> {
    @Autowired
    private IApplicationRepository applicationRepository;

    @Override
    public List<Application> get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
        return applicationRepository.findAll();
    }
}
