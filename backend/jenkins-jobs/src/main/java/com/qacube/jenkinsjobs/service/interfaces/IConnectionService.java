package com.qacube.jenkinsjobs.service.interfaces;

import com.qacube.jenkinsjobs.domain.Connection;
import com.qacube.jenkinsjobs.domain.dto.ConnectionDto;

import java.util.List;

public interface IConnectionService {
    Connection create(Connection connection);
    List<ConnectionDto> findAll();
    ConnectionDto authentication(Long id, String username, String password);
    Connection findById(Long id);
    Connection update(Connection updateConnection);
    void delete(Long id);
    void deleteConnectionsByApplication(Long id);
}
