package com.qacube.jenkinsjobs.controller;

import com.qacube.jenkinsjobs.domain.Application;
import com.qacube.jenkinsjobs.service.provider.GraphQLProvider;
import com.qacube.jenkinsjobs.service.interfaces.IApplicationService;
import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/app")
@CrossOrigin(origins = "http://localhost:4200")
public class ApplicationController {

    @Autowired
    private IApplicationService applicationService;

    @GetMapping
    public List<Application> findAll(){
        return applicationService.findAll();
    }

    @GetMapping("/{id}")
    public Application findById(@PathVariable("id") Long id){
        return applicationService.findById(id);
    }

    @PostMapping
    public Application create(@RequestBody Application application){
        return applicationService.create(application);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") long id){
        applicationService.delete(id);
    }

    @PutMapping
    public Application update(@RequestBody Application updateApplication) {
        return applicationService.update(updateApplication);
    }


    // GraphQL

    @Autowired
    private GraphQLProvider graphQLProvider;

    @PostMapping("/graphql")
    public ResponseEntity<Object> findAll(@RequestBody String query){
        ExecutionResult execute = graphQLProvider.graphQL().execute(query);
        return new ResponseEntity<>(execute, HttpStatus.OK);
    }


}
