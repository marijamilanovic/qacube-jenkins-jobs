package com.qacube.jenkinsjobs.mapper;

import com.qacube.jenkinsjobs.domain.Connection;
import com.qacube.jenkinsjobs.domain.dto.ConnectionDto;

public class ConnectionMapper {
    public static ConnectionDto mapConnectionToConnectionDto(Connection connection, String applicationName) {
        ConnectionDto connectionDto = new ConnectionDto();
        connectionDto.setId(connection.getId());
        connectionDto.setName(connection.getName());
        connectionDto.setUrl(connection.getUrl());
        connectionDto.setApplicationName(applicationName);
        connectionDto.setEnableAuthentication(connection.isEnableAuthentication());
        connectionDto.setUsername(connection.getUsername());
        connectionDto.setPassword(connection.getPassword());
        return connectionDto;
    }

    public static Connection mapConnectionDtoToConnection(ConnectionDto connectionDto, Long applicationId) {
        Connection connection = new Connection();
        connection.setId(connectionDto.getId());
        connection.setName(connectionDto.getName());
        connection.setUrl(connectionDto.getUrl());
        connection.setApplicationId(applicationId);
        connection.setEnableAuthentication(connectionDto.isEnableAuthentication());
        connection.setUsername(connectionDto.getUsername());
        connection.setPassword(connectionDto.getPassword());
        return connection;
    }
}
