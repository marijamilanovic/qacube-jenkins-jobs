package com.qacube.jenkinsjobs.domain.dto;

import lombok.Data;

@Data
public class BranchDto {
    private String name;
}
