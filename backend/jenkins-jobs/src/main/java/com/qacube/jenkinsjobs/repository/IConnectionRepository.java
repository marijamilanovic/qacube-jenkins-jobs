package com.qacube.jenkinsjobs.repository;

import com.qacube.jenkinsjobs.domain.Connection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IConnectionRepository extends JpaRepository<Connection, Long> {
    Connection save(Connection connection);
    List<Connection> findAll();
    Connection findConnectionById(Long id);
    List<Connection> findConnectionByApplicationId(Long id);
    Connection findConnectionByName(String name);
    Connection findConnectionByUrl(String url);
}
