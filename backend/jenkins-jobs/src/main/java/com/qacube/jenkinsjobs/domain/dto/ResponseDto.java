package com.qacube.jenkinsjobs.domain.dto;


import lombok.Data;

import java.util.List;

@Data
public class ResponseDto {
    private List<JobDto> jobs;
}
