package com.qacube.jenkinsjobs.service.interfaces;

import com.qacube.jenkinsjobs.domain.Application;

import java.util.List;

public interface IApplicationService {
    Application create(Application application);
    List<Application> findAll();
    Application findById(Long id);
    void delete(Long id);
    Application update(Application updateApplication);
}
