package com.qacube.jenkinsjobs.domain;

import lombok.Data;

import java.util.Date;

@Data
public class Notification {
    private String text;
    private Date time;

    public Notification(String text, Date time) {
        super();
        this.text = text;
        this.time = time;
    }
}
