package com.qacube.jenkinsjobs.service;

import com.qacube.jenkinsjobs.domain.Connection;
import com.qacube.jenkinsjobs.domain.dto.JobDto;
import com.qacube.jenkinsjobs.domain.dto.ResponseDto;
import com.qacube.jenkinsjobs.exception.NotFoundException;
import com.qacube.jenkinsjobs.service.interfaces.IConnectionService;
import com.qacube.jenkinsjobs.service.interfaces.IJobService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class JobService implements IJobService {
    @Autowired
    private IConnectionService connectionService;
    private static String theUrl = "/api/json?depth=2&tree=jobs[name,lastBuild[result,building,duration,timestamp,actions[lastBuiltRevision[SHA1,branch[name]]]]]&pretty=true";

    @Override
    public List<JobDto> jobs(Long connectionId, String username, String password) {
        Connection connection = connectionService.findById(connectionId);
        if(connection != null){
            String base64Creds = new String(Base64.encodeBase64((username + ":" + password).getBytes()));
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Basic " + base64Creds);
            try {
                return getJobs(headers, connection);
            }
            catch (Exception eek) {
                System.out.println("Exception: "+ eek.getMessage());
            }
        }
        throw new NotFoundException("Not found job.");
    }

    @Override
    public List<JobDto> jobs(Long connectionId) {
        Connection connection = connectionService.findById(connectionId);
        if(connection != null){
            try {
                return getJobs(new HttpHeaders(), connection);
            }
            catch (Exception eek) {
                System.out.println("Exception: "+ eek.getMessage());
            }
        }
        throw new NotFoundException("Not found job.");
    }


    public List<JobDto> getJobs(HttpHeaders headers, Connection connection){
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<ResponseDto> res = restTemplate.exchange(connection.getUrl() + theUrl, HttpMethod.GET, entity, ResponseDto.class, 1);
        return res.getBody().getJobs();
    }
}
