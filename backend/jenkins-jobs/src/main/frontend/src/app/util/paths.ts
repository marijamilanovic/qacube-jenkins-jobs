export const BACKEND_PATH = 'http://localhost:6060';
// ---------------------------------------------------

export const APPLICATION_PATH = BACKEND_PATH + '/app';

export const CONNECTION_PATH = BACKEND_PATH + '/connection';

export const JOB_PATH = BACKEND_PATH + '/job';
