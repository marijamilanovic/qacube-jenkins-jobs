import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/components/navbar/application/application.service';
import { ConnectionService } from 'src/app/components/navbar/connection/connection.service';
import { Application } from '../../application/application.model';
import { Connection } from '../connection.model';

@Component({
  selector: 'app-new-connection',
  templateUrl: './new-connection.component.html',
  styleUrls: ['./new-connection.component.css']
})
export class NewConnectionComponent implements OnInit {

  connection: { id: number; name: string; url: string; username: string; password: string;
    applicationId: number; enableAuthentication: boolean} = new Connection();
  selectedApp: any;
  allApps: Application[];
  application: { id: number; name: string} = new Application();
  constructor(private router: Router, private toastrService: ToastrService,
              private connectionService: ConnectionService, private applicationService: ApplicationService) {
                this.allApps = [];
                this.connection = { id: 0, name: '', url: '', username: '', password: '', applicationId: 0, enableAuthentication: false};
                this.application = {id: 0, name: ''};
              }

  ngOnInit(): void {
    this.applicationService.getAllApps().subscribe( (data: any[]) => {
      for (const app of data) {
        this.application = new Application().deserialize(app);
        if (this.application instanceof Application) {
          this.allApps.push(this.application);
        }
        if (this.application.id === this.connection.applicationId) {
          this.selectedApp = this.application.name;
        }
      }
      console.log(this.allApps);
    });
  }

  addNewConnection(): number{
    this.connection.applicationId =  this.selectedApp;
    if ((this.connection.enableAuthentication && (this.connection.username === '' || this.connection.password === ''))
    || (this.connection.name === '' || this.connection.url === '' || this.connection.applicationId === null)){
      this.toastrService.error('Please fill out all fields', 'Error');
      return 0;
    }
    this.connectionService.addNewConnection(this.connection).subscribe(data => {
      this.toastrService.success('Added new connection.', 'Success');
      this.router.navigate(['/all-connections']);
    }, error => {
      this.toastrService.error('Error occurred.');
    });
    return 0;
  }

  back(): void{
    this.router.navigate(['all-connections']);
  }

}
