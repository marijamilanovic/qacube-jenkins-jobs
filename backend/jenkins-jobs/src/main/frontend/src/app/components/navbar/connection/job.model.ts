import { Deserializable } from 'src/app/util/deserializable.model';

export class Job implements Deserializable {
    name: string;
    lastBuild: LastBuild;
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}

export class LastBuild implements Deserializable {
    result: any;
    timestamp: any;
    duration: any;
    actions: Action[];
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}

export class Action implements Deserializable {
    lastBuiltRevision: Branch[];
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}

export class Branch implements Deserializable {
    name: string;
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}
