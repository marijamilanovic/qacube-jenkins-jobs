import { Component, OnInit } from '@angular/core';
import { Message } from '@stomp/stompjs';
import { StompState } from '@stomp/ng2-stompjs';
import { NotificationService } from 'src/app/notification/notification.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';

const WEBSOCKET_URL = 'ws://localhost:6060/socket';
const EXAMPLE_URL = '/topic/server-broadcaster';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private messagingService: NotificationService;
  notify: any;

  constructor(private toastrService: ToastrService, private datePipe: DatePipe) {
    // Instantiate a messagingService
    this.messagingService = new NotificationService(WEBSOCKET_URL, EXAMPLE_URL);

    this.messagingService.stream().subscribe((message: Message) => {
      this.notify = JSON.parse(message.body);
      this.toastrService.info(this.notify.text, this.datePipe.transform(this.notify.time, 'M/d/yy, h:mm a'));
      console.log(this.notify);
    });
  }

  ngOnInit(): void {
  }

}
