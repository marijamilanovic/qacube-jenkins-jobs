import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/components/navbar/application/application.service';
import { Application } from '../application.model';

@Component({
  selector: 'app-all-apps',
  templateUrl: './all-apps.component.html',
  styleUrls: ['./all-apps.component.css']
})
export class AllAppsComponent implements OnInit {

apps: Application[];
application: { id: number; name: string} = new Application();

  constructor(private appService: ApplicationService,  private toastrService: ToastrService,
              private router: Router) {
              this.apps = [];
              this.application = {id: 0, name: ''};
   }

  ngOnInit(): void {
    this.appService.getAllApps().subscribe( (data: any) => {
      for (const app of data) {
        this.application = new Application().deserialize(app);
        if (this.application instanceof Application) {
          this.apps.push(this.application);
        }
      }
    });
  }

  deleteApplication(id: any): void{
    this.appService.deleteApplication(id).subscribe(data => {
      this.toastrService.success('Application has been deleted.', 'Success');
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
        this.router.navigate(['apps/all']);
      });
    }, error => {
      this.toastrService.error('Error occurred.', 'Error');
    });
  }

  addNewApplication(): void{
    this.router.navigate(['apps/new']);
  }
}
