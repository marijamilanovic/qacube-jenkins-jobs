import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONNECTION_PATH } from '../../../util/paths';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  constructor(private httpClient: HttpClient) { }

  getAllConnections(): any{
    return this.httpClient.get(CONNECTION_PATH);
  }

  addNewConnection(connection: any): any{
    return this.httpClient.post(CONNECTION_PATH, connection);
  }

  getConnection(id: any): any{
    return this.httpClient.get(CONNECTION_PATH + '/' + id);
  }

  updateConnection(connection: any): any{
    return this.httpClient.put(CONNECTION_PATH, connection);
  }

  deleteConnection(id: number): any{
    return this.httpClient.delete(CONNECTION_PATH + '/delete/' + id);
  }

  doAuthentication(id: number, username: string, password: string): any{
    return this.httpClient.get(CONNECTION_PATH + '/' + id + '/' + username + '/' + password);
  }

  getConnectionGraphQL(text: string): any{
    return this.httpClient.post(CONNECTION_PATH + '/graphql', text);
  }
}
