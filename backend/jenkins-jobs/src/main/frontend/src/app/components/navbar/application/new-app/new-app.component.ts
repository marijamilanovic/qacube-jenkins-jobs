import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/components/navbar/application/application.service';
import { Application } from '../application.model';

@Component({
  selector: 'app-new-app',
  templateUrl: './new-app.component.html',
  styleUrls: ['./new-app.component.css']
})
export class NewAppComponent implements OnInit {

application: { id: number; name: string} = new Application();

  constructor(private router: Router, private toastrService: ToastrService, private applicationService: ApplicationService) {
    this.application = {id: 0, name: ''};
   }

  ngOnInit(): void {
  }

  addNewApplication(): void{
    this.applicationService.addNewApplication(this.application).subscribe(data => {
      this.application = new Application().deserialize(data);
      console.log(this.application);
      this.toastrService.success('Added new application.', 'Success');
      this.router.navigate(['apps/all']);
    }, error => {
      this.toastrService.error('Error occured. Application with that name already exist.');
    });
  }

  back(): void{
    this.router.navigate(['apps/all']);
  }
}
