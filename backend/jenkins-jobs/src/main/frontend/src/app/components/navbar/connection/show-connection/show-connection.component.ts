import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/components/navbar/application/application.service';
import { ConnectionService } from 'src/app/components/navbar/connection/connection.service';
import { JobService } from 'src/app/components/navbar/connection/job.service';
import { Application } from '../../application/application.model';
import { Authentication } from '../authentication.model';
import { Connection } from '../connection.model';
import { Action, Branch, Job, LastBuild } from '../job.model';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-show-connection',
  templateUrl: './show-connection.component.html',
  styleUrls: ['./show-connection.component.css']
})
export class ShowConnectionComponent implements OnInit {

  id: any;
  username: any;
  password: any;
  connection: { id: number; name: string; url: string; username: string; password: string;
    applicationId: number; enableAuthentication: boolean} = new Connection();
  jobs: Job[];
  job: { name: string; lastBuild: LastBuild } = new Job();
  lastBuild: { result: any, timestamp: any, duration: any; actions: Action[]} = new LastBuild();
  action: { lastBuiltRevision: Branch[]} = new Action();
  branch: { name: string} = new Branch();
  mustLogin: boolean;
  authentication: { id: number, username: string, password: string} = new Authentication();
  allApps: Application[];
  application: { id: number; name: string} = new Application();
  closeModal: string;
  text: string;
  responseGraphQL: any;
  showResult: boolean;

  constructor(private activatedRoute: ActivatedRoute, private connectionService: ConnectionService,
              private modalService: NgbModal, private router: Router,
              private toastrService: ToastrService, private jobService: JobService) {
                this.connection = { id: 0, name: '', url: '', username: '', password: '', applicationId: 0, enableAuthentication: false};
                this.application = {id: 0, name: ''};
                this.allApps = [];
                this.authentication = {id: 0, username: '', password: ''};
                this.job = { name: '', lastBuild: new LastBuild() };
                this.lastBuild = { result: '', timestamp: '', duration: '', actions: []};
                this.action = { lastBuiltRevision: []};
                this.branch = { name: ''};
                this.showResult = false;
              }

  ngOnInit(): void {
    this.authentication = new Authentication();
    this.jobs = [];
    this.mustLogin = false;
    this.activatedRoute.paramMap.subscribe( params => {
      this.id = params.get('id');
      this.connectionService.getConnection(this.id).subscribe(data => {
        this.connection = new Connection().deserialize(data);
        this.username = this.connection.username;
        this.password = this.connection.password;
        if (this.connection.enableAuthentication) {
          this.mustLogin = true;
        }else {
          this.getJobsPublic();
        }
      });
    });
  }

  doAuthentication(): void{
    this.authentication.id =  this.id;
    this.connectionService.doAuthentication(this.authentication.id, this.authentication.username, this.authentication.password)
      .subscribe(data => {
      this.toastrService.success('', 'Success');
      this.mustLogin = false;
      this.getJobs();
    }, error => {
      this.toastrService.error('Error occurred.', 'Error');
    });
  }

  back(): void{
    this.router.navigate(['all-connections']);
  }

  getJobs(): void{
    this.jobService.getAllJobs(this.id, this.username, this.password).subscribe( (data: any) => {
      for (const j of data) {
        this.job = new Job().deserialize(j);
        if (this.job instanceof Job) {
          this.jobs.push(this.job);
        }
      }
    });
  }
  getJobsPublic(): void{
    this.jobService.getAllJobsPublic(this.id).subscribe((data: any) => {
      for (const j of data) {
        this.job = new Job().deserialize(j);
        if (this.job instanceof Job) {
          this.jobs.push(this.job);
        }
      }
      console.log(this.jobs);
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  openDialog(content, id): void {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
    this.showResult = true;
    this.text = '{connectionById(id: ' + id + ') {id name enableAuthentication username password url applicationId}}';
    this.connectionService.getConnectionGraphQL(this.text).subscribe( (data: any) => {
      console.log('RESENJE');
      console.log(data.data);
      this.responseGraphQL = data.data;
    });
  }
}
