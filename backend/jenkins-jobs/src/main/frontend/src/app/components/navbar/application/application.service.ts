import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APPLICATION_PATH } from '../../../util/paths';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private httpClient: HttpClient) { }

  getAllApps(): any{
    return this.httpClient.get(APPLICATION_PATH);
  }

  deleteApplication(id: any): any{
    return this.httpClient.delete(APPLICATION_PATH + '/delete/' + id);
  }

  addNewApplication(application: any): any{
    return this.httpClient.post(APPLICATION_PATH, application);
  }

  updateApplication(application: any): any{
    return this.httpClient.put(APPLICATION_PATH, application);
  }

  getApplication(id: any): any{
    return this.httpClient.get(APPLICATION_PATH + '/' + id);
  }
}
