import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConnectionService } from 'src/app/components/navbar/connection/connection.service';
import { Connection } from '../connection.model';

@Component({
  selector: 'app-all-connections',
  templateUrl: './all-connections.component.html',
  styleUrls: ['./all-connections.component.css']
})
export class AllConnectionsComponent implements OnInit {

connections: Connection[];
connection: { id: number; name: string; url: string; username: string; password: string;
              applicationId: number; enableAuthentication: boolean} = new Connection();

  constructor(private connectionService: ConnectionService, private toastrService: ToastrService,
              private router: Router) {
              this.connections = [];
              this.connection = { id: 0, name: '', url: '', username: '', password: '', applicationId: 0, enableAuthentication: false};
  }

  ngOnInit(): void {
    this.connectionService.getAllConnections().subscribe( (data: any) => {
      for (const conn of data) {
        this.connection = new Connection().deserialize(conn);
        if (this.connection instanceof Connection) {
          this.connections.push(this.connection);
        }
      }
      console.log(this.connections);
    });
  }

  deleteConnection(id: number): void{
    this.connectionService.deleteConnection(id).subscribe(data => {
      this.toastrService.success('Connection has been deleted.', 'Success');
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
        this.router.navigate(['all-connections']);
      });
    }, error => {
      this.toastrService.error('Error occurred.', 'Error');
    });
  }

  addNewConnection(): void{
    this.router.navigate(['new-connection']);
  }

}
