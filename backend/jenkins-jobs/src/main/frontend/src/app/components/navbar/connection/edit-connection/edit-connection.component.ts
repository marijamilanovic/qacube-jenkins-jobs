import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/components/navbar/application/application.service';
import { ConnectionService } from 'src/app/components/navbar/connection/connection.service';
import { Application, Applications } from '../../application/application.model';
import { Authentication } from '../authentication.model';
import { Connection } from '../connection.model';

@Component({
  selector: 'app-edit-connection',
  templateUrl: './edit-connection.component.html',
  styleUrls: ['./edit-connection.component.css']
})
export class EditConnectionComponent implements OnInit {

  id: any;
  connection: { id: number; name: string; url: string; username: string; password: string;
    applicationId: number; enableAuthentication: boolean} = new Connection();
  selectedApp: any;
  allApps: Application[];
  application: { id: number; name: string} = new Application();
  mustLogin: boolean;
  authentication: { id: number, username: string, password: string} = new Authentication();

  constructor(private activatedRoute: ActivatedRoute, private connectionService: ConnectionService,
              private applicationService: ApplicationService, private router: Router,
              private toastrService: ToastrService) {
                this.connection = { id: 0, name: '', url: '', username: '', password: '', applicationId: 0, enableAuthentication: false};
                this.application = {id: 0, name: ''};
                this.allApps = [];
                this.authentication = {id: 0, username: '', password: ''};
              }

  ngOnInit(): void {
    this.mustLogin = false;
    this.activatedRoute.paramMap.subscribe( params => {
      this.id = params.get('id');
      this.connectionService.getConnection(this.id).subscribe(data => {
        this.connection = new Connection().deserialize(data);
        if (this.connection.enableAuthentication){
          this.mustLogin = true;
        }
      });
    });
    this.applicationService.getAllApps().subscribe( (data: any[]) => {
      for (const app of data) {
        this.application = new Application().deserialize(app);
        if (this.application instanceof Application) {
          this.allApps.push(this.application);
        }
        if (this.application.id === this.connection.applicationId) {
          this.selectedApp = this.application.name;
        }
      }
      console.log(this.allApps);
    });
}

  updateConnection(): number{
    console.log(this.connection);
    this.connection.applicationId =  this.selectedApp;
    if ((this.connection.enableAuthentication && (this.connection.username === '' || this.connection.password === ''))
    || (this.connection.name === '' || this.connection.url === '' || this.connection.applicationId === null)){
      this.toastrService.error('Please fill out all fields', 'Error');
      return 0;
    }
    this.connectionService.updateConnection(this.connection).subscribe(data => {
      this.toastrService.success('Updated connection.', 'Success');
      this.router.navigate(['all-connections']);
    }, error => {
      this.toastrService.error('Error occurred.', 'Error');
    });
    return 0;
  }

  back(): void{
    this.router.navigate(['all-connections']);
  }

  doAuthentication(): void{
    this.connectionService.doAuthentication(this.id, this.authentication.username, this.authentication.password).subscribe(data => {
      this.toastrService.success('Successfully logged in.', 'Success');
      this.mustLogin = false;
    }, error => {
      this.toastrService.error('Error occurred.', 'Error');
    });
  }

}
