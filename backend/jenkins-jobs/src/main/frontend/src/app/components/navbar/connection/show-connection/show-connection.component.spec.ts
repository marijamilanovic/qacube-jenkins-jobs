import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowConnectionComponent } from './show-connection.component';

describe('ShowConnectionComponent', () => {
  let component: ShowConnectionComponent;
  let fixture: ComponentFixture<ShowConnectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowConnectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
