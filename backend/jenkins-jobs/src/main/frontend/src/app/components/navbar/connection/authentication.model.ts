import { Deserializable } from 'src/app/util/deserializable.model';

export class Authentication implements Deserializable {
    id: number;
    username: string;
    password: string;
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}
