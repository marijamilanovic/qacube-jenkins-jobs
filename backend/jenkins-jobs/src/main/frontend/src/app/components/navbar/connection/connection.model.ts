import { Deserializable } from 'src/app/util/deserializable.model';

export class Connection implements Deserializable {
    id: number;
    name: string;
    url: string;
    username: string;
    password: string;
    applicationId: number;
    enableAuthentication: boolean;
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}
