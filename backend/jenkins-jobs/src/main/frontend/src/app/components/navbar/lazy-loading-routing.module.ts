import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// components
import { AllAppsComponent } from './application/all-apps/all-apps.component';
import { NewAppComponent } from './application/new-app/new-app.component';
import { EditAppComponent } from './application/edit-app/edit-app.component';


const routes: Routes = [
  { path: 'all', component: AllAppsComponent },
  { path: 'new', component: NewAppComponent },
  { path: 'all/edit/:id', component: EditAppComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyLoadingRoutingModule { }
