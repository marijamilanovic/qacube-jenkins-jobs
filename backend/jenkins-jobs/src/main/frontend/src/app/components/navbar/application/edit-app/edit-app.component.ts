import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/components/navbar/application/application.service';
import { Application } from '../application.model';

@Component({
  selector: 'app-edit-app',
  templateUrl: './edit-app.component.html',
  styleUrls: ['./edit-app.component.css']
})
export class EditAppComponent implements OnInit {

  id: any;
  application: { id: number; name: string} = new Application();

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
              private toastrService: ToastrService, private appService: ApplicationService) {
                this.application = {id: 0, name: ''};
               }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.appService.getApplication(this.id).subscribe(data => {
        this.application = new Application().deserialize(data);
      });
    });
  }

  updateApplication(): void{
    this.appService.updateApplication(this.application).subscribe(data => {
      this.toastrService.success('Updated application.', 'Success');
      this.router.navigate(['apps/all']);
    }, error => {
      this.toastrService.error('Error occurred. Application with that name already exist.', 'Error');
    });
  }

  back(): void{
    this.router.navigate(['apps/all']);
  }

}
