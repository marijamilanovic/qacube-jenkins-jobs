import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JOB_PATH } from '../../../util/paths';

export class Job{
  constructor(
    public jobs: string
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private httpClient: HttpClient) { }


  getAllJobs(id: any, username: any, password: any): any{
    return this.httpClient.get(JOB_PATH + '/' + id + '/' + username + '/' + password);
  }

  getAllJobsPublic(id: any): any{
    return this.httpClient.get(JOB_PATH + '/' + id);
  }
}
