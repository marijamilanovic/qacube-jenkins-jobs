import { Deserializable } from '../../../util/deserializable.model';

export class Application implements Deserializable {
    id: number;
    name: string;
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}

export class Applications implements Deserializable {
    application: Application[];
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}
