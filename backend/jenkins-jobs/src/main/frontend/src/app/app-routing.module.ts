import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// components
import { HomeComponent } from './components/navbar/home/home.component';
import { AllConnectionsComponent } from './components/navbar/connection/all-connections/all-connections.component';
import { NewConnectionComponent } from './components/navbar/connection/new-connection/new-connection.component';
import { ShowConnectionComponent } from './components/navbar/connection/show-connection/show-connection.component';
import { EditConnectionComponent } from './components/navbar/connection/edit-connection/edit-connection.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'apps',
    loadChildren: () => import ('./components/navbar/lazy-loading.module').then(m => m.LazyLoadingModule)},
  { path: 'all-connections', component: AllConnectionsComponent },
  { path: 'new-connection', component: NewConnectionComponent},
  { path: 'show-connection/:id', component: ShowConnectionComponent },
  { path: 'edit-connection/:id', component: EditConnectionComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
