import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';


// components
import { AllAppsComponent } from './components/navbar/application/all-apps/all-apps.component';
import { HomeComponent } from './components/navbar/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NewAppComponent } from './components/navbar/application/new-app/new-app.component';
import { AllConnectionsComponent } from './components/navbar/connection/all-connections/all-connections.component';
import { NewConnectionComponent } from './components/navbar/connection/new-connection/new-connection.component';
import { EditConnectionComponent } from './components/navbar/connection/edit-connection/edit-connection.component';
import { EditAppComponent } from './components/navbar/application/edit-app/edit-app.component';
import { ShowConnectionComponent } from './components/navbar/connection/show-connection/show-connection.component';
import { GraphQLModule } from './graphql.module';

@NgModule({
  declarations: [
    AppComponent,
    AllAppsComponent,
    HomeComponent,
    NavbarComponent,
    NewAppComponent,
    AllConnectionsComponent,
    NewConnectionComponent,
    EditConnectionComponent,
    EditAppComponent,
    ShowConnectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    NgbModule,
    GraphQLModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
