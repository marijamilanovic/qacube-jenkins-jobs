-- Application
insert into application (name) values ("APP1");
insert into application (name) values ("APP2");
insert into application (name) values ("APP3");
insert into application (name) values ("APP4");
insert into application (name) values ("APP5");

-- Connection
insert into connection (name ,url, username, password, application_id, enable_authentication) values ("QACube Jenkins Jobs", "http://localhost:8000", "masha", "masha", 1, true);
insert into connection (name ,url, username, password, application_id, enable_authentication) values ("Builds Apache", "https://builds.apache.org", "", "", 2, false);