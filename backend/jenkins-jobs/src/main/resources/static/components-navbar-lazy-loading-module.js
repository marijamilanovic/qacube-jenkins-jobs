(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-navbar-lazy-loading-module"],{

/***/ "Z87q":
/*!**********************************************************!*\
  !*** ./src/app/components/navbar/lazy-loading.module.ts ***!
  \**********************************************************/
/*! exports provided: LazyLoadingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LazyLoadingModule", function() { return LazyLoadingModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _lazy_loading_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lazy-loading-routing.module */ "pH+e");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");



class LazyLoadingModule {
}
LazyLoadingModule.ɵfac = function LazyLoadingModule_Factory(t) { return new (t || LazyLoadingModule)(); };
LazyLoadingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: LazyLoadingModule });
LazyLoadingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _lazy_loading_routing_module__WEBPACK_IMPORTED_MODULE_1__["LazyLoadingRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](LazyLoadingModule, { imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _lazy_loading_routing_module__WEBPACK_IMPORTED_MODULE_1__["LazyLoadingRoutingModule"]] }); })();


/***/ }),

/***/ "pH+e":
/*!******************************************************************!*\
  !*** ./src/app/components/navbar/lazy-loading-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: LazyLoadingRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LazyLoadingRoutingModule", function() { return LazyLoadingRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _application_all_apps_all_apps_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./application/all-apps/all-apps.component */ "dZ9w");
/* harmony import */ var _application_new_app_new_app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./application/new-app/new-app.component */ "XikE");
/* harmony import */ var _application_edit_app_edit_app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./application/edit-app/edit-app.component */ "1SgS");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");

// components





const routes = [
    { path: 'all', component: _application_all_apps_all_apps_component__WEBPACK_IMPORTED_MODULE_1__["AllAppsComponent"] },
    { path: 'new', component: _application_new_app_new_app_component__WEBPACK_IMPORTED_MODULE_2__["NewAppComponent"] },
    { path: 'all/edit/:id', component: _application_edit_app_edit_app_component__WEBPACK_IMPORTED_MODULE_3__["EditAppComponent"] },
];
class LazyLoadingRoutingModule {
}
LazyLoadingRoutingModule.ɵfac = function LazyLoadingRoutingModule_Factory(t) { return new (t || LazyLoadingRoutingModule)(); };
LazyLoadingRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({ type: LazyLoadingRoutingModule });
LazyLoadingRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](LazyLoadingRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ })

}]);
//# sourceMappingURL=components-navbar-lazy-loading-module.js.map