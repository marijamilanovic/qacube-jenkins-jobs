# QACube Jenkins Jobs

Trello: https://trello.com/b/BPkCHgq2/jenkins-jobs

### Microservices:
| Name | Port | About
| ------ | ------ | ------ | 
| Front service | 4200 | ... |
| Back service | 6060 | ... | 
| Jenkins service | 8000 | Used for CI and CD solution | 
	


Technologies: Spring-Boot, Angular, MySQL database

Database instructions: https://www.sqlshack.com/how-to-install-mysql-database-server-8-0-19-on-windows-10/

Jenkins: 
1. https://www.blazemeter.com/blog/how-to-install-jenkins-on-windows (port:8000)


Backend:
1. Download and install Intellij: https://www.jetbrains.com/idea/download/#section=windows
2. Open project in Intellij
3. Build project
4. Run (port:6060) --> ready for production




